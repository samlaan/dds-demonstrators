.. (C) ALbert Mietus, Sogeti, 2019

.. _MBC:

==========================
Matrix Board Communication
==========================

This DDS-Demonstrator is inspired by the communication of the well know “Matrix Boards” above the (Dutch) Highways.
|BR|
Each MatrixBoard shows the maximal allowed speed; during traffic-jams. It needs the input from the next two boards; whose
sensors measure the speed at that location.

The real MatrixBoards have sensors *(to measure the number of cars, there speed, etc)*, an actuators *(showing the
speed)*, and some simple algorithms (like: *“the step is speed between 2 boards is small”* and *“when the speed changes,
the light should blink for some time”*). The  communication between board is are hard-wired; by using land-lans.

.. demo:: MatrixBoard
   :ID: MBC_001
   :tags: MBC
   :status: import

   The MatrixBoard DDS-Demonstrator shows some dynamic aspects of DDS in a similar setup. It uses DDS for all
   communication.
   |BR|
   It does not implement the real MatrixBoards: it has no sensors, no actuators, nor the exact algorithms.  The former
   two are (very simple) simulated; and the algorithms are replaces by simple rules (roughly doing the same).

   When running a set of MatrixBoards, each MatrixBoard is started with a location (thinks about a “hectometer sign”).
   |BR|
   It should automatically “find” the next two ‘upstream’ MatrixBoards and *subscribe* to those; only.

   Whenever the set of MatrixBoards changes, a MatrixBoard should automatically discover the new setup and act
   accordingly (cancel and add subscriptions)


Example
-------

Given a setup like below, where the MatrixBoard at  ``loc=3`` does not yet exist; and ``loc=21`` does.

* MatrixBoard at ``loc=1`` should subscribe to ``loc=2``  and ``loc=5``.
* MatrixBoard at ``loc=8`` should subscribe to ``loc=13`` and ``loc=21``.

.. uml::

   @startditaa

   /---------\   /---------\   /=--------\   /---------\   /---------\   /---------\   /---------\   /---------\
   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |   | Matrix  |
   |         |   |         |   |         |   |         |   |         |   |         |   |         |   |         |
   |  loc=1  |   |  loc=2  |   |  loc=3  |   |  loc=5  |   |  loc=8  |   | loc=13  |   | loc=21  |   | loc=34  |
   |    c1F7 |   |    c1FF |   |         |   |    c1FF |   |    c1F7 |   |    c1FF |   |    cFDD |   |    c1FF |
   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/   \----+----/
        |             |             |             |             |             |             |             |
      --*-------------*-------------*-------------*-------------*-------------*-------------*-------------*-----> drive
   @endditaa


Now suppose, MatrixBoard ``loc=3`` is added and/or ``loc=21`` is abruptly killed. Then:

* MatrixBoard at ``loc=1`` will use ``loc=2`` and ``loc=3`` (and **not** listen to ``loc=5`` anymore).
* MatrixBoard at ``loc=8`` will add ``loc=34`` to is subscription-list.



.. toctree::
   :glob:
   :maxdepth: 0

   *



